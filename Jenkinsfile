def getFolderName() {
  def array = pwd().split("/")
  return array[array.length - 2];
}
pipeline {
  agent any
  environment {
    BRANCHES = "${env.GIT_BRANCH}"
    COMMIT = "${env.GIT_COMMIT}"
    RELEASE_NAME = "kafka"
    SERVICE_PORT = "${APP_PORT}"
    DOCKERHOST = "${DOCKERHOST_IP}"
    ACTION = "${ACTION}"
    DEPLOYMENT_TYPE = "${DEPLOYMENT_TYPE == ""? "EC2":DEPLOYMENT_TYPE}"
    KUBE_SECRET = "${KUBE_SECRET}"
    foldername = getFolderName()

  }
  stages {
    stage('init') {
      steps {
        script {

          def job_name = "$env.JOB_NAME"
          print(job_name)
          def values = job_name.split('/')
          namespace_prefix = values[0].replaceAll("[^a-zA-Z0-9]+","").toLowerCase().take(50)
          namespace = "$namespace_prefix-$env.foldername".toLowerCase()
          service = values[2].replaceAll("[^a-zA-Z0-9]+","").toLowerCase().take(50)
          print("kube namespace: $namespace")
          print("service name: $service")
          env.namespace_name=namespace
          env.service=service
        }
      }
    }

    stage('Deploy') {

      steps {
        script {
          echo "echoed folder--- $foldername"

          if (env.DEPLOYMENT_TYPE == 'EC2') {

            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop kafka zookeeper || true && docker rm kafka zookeeper || true"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker run -d --name zoo -p 2181:2181 zookeeper"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker run -d --name kafka -p 9092:9092 -p 19092:19092 --hostname kafka  -e KAFKA_DOCKER_IP=$DOCKERHOST --link zoo:zoo public.ecr.aws/lazsa/kafka:latest"'

            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "sleep 5s"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop kafdrop || true && docker rm kafdrop || true"'
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker run -d --name kafdrop -p 9093:9000 --link kafka:kafka -e KAFKA_BROKERCONNECT=kafka:9092 obsidiandynamics/kafdrop"'


          }
          if (env.DEPLOYMENT_TYPE == 'KUBERNETES') {

            withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG')]) {
                  sh '''


                    helm repo add bitnami https://charts.bitnami.com/bitnami || true
                    kubectl create ns "$namespace_name" || true

                    helm upgrade --install $RELEASE_NAME -n "$namespace_name" bitnami/kafka -f values-production.yaml --atomic --timeout 300s
                    sleep 10
                    export kafka_server="kafka-0\\.kafka-headless\\.$namespace_name\\.svc\\.cluster\\.local\\:9092\\,kafka-1\\.kafka-headless\\.$namespace_name\\.svc\\.cluster\\.local\\:9092\\,kafka-2\\.kafka-headless\\.$namespace_name\\.svc\\.cluster\\.local\\:9092"

                    helm upgrade -i kafdrop kafdrop -n "$namespace_name" --set image.tag=3.27.0 \
                        --set kafka.brokerConnect="$kafka_server" \
                        --set server.servlet.contextPath="/" \
                        --set cmdArgs="--message.format=AVRO --schemaregistry.connect=http://localhost:8080" \
                        --set jvm.opts="-Xms32M -Xmx64M"

                    sleep 10
                  '''
                  script {
                    env.temp_service_name1 = "kafdrop"
                    env.temp_service_name2 = "kafka"
                    def url1 = sh (returnStdout: true, script: '''kubectl get svc -n "$namespace_name" | grep "$temp_service_name1" | awk '{print $4}' ''').trim()
                    def url2 = sh (returnStdout: true, script: '''kubectl get svc -n "$namespace_name" | grep "$temp_service_name2" | awk '{print $4}' ''').trim()
                    if (url1 != "<pending>") {
                      print("##\$@\$ http://$url1 ##\$@\$")
                    }
                    if (url2 != "<pending>") {
                      print("##\$@\$ http://$url2 ##\$@\$")
                    }
                  }
            }
          }
        }
      }
    }

    stage('Destroy') {
      when {
        expression {
          env.DEPLOYMENT_TYPE == 'EC2' && env.ACTION == 'DESTROY'
        }
      }
      steps {
        script {
          if (env.DEPLOYMENT_TYPE == 'EC2') {
            sh 'ssh -o "StrictHostKeyChecking=no" ciuser@$DOCKERHOST "docker stop mongodb mongo-express || true && docker rm mongodb mongo-express || true"'
          }
          if (env.DEPLOYMENT_TYPE == 'KUBERNETES') {
            withCredentials([file(credentialsId: "$KUBE_SECRET", variable: 'KUBECONFIG')]) {
                  sh '''
                    helm uninstall $RELEASE_NAME -n "$namespace_name"
                    helm uninstall $RELEASE_NAME-express -n "$namespace_name"
                  '''
            }
          }
        }
      }
    }
  }
}
